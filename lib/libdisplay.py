from constants import *

import copy
import time
import pygame
import pygame.display


# Initialise PyGame (can be called more than once.)
pygame.init()
pygame.display.init()
pygame.font.init()


# # # # #
# CLASSES

class MagicDisplay:
    
    def __init__(self, dispsize, fullscreen=True):
        
        # Generate display Surface.
        if fullscreen:
            flags = pygame.FULLSCREEN
        self._disp = pygame.display.set_mode(dispsize, flags)
        self._disp.fill(BGC)
        
        # Generate a Font.
        self._fontfile = pygame.font.get_default_font()
        self._font = {}
        self._font[24] = pygame.font.Font(self._fontfile, 24)
    
    
    def clear(self):
        
        self._disp.fill(BGC)
        pygame.display.flip()
    
    
    def close(self):
        pygame.display.quit()
    
    
    def draw_border(self, colour):
        
        w, h = self._disp.get_size()
        pl = [(0,0), (0,h), (w,h), (w,0), (0,0)]
        pygame.draw.lines(self._disp, colour, False, pl, int(HORMARGIN/2))
        pygame.display.flip()
    
    
    def draw_text(self, text, highlight_words = [], fontsize=50):
        
        dispsize = self._disp.get_size()
        textrect = (HORMARGIN, VERMARGIN, \
            dispsize[0]-(2*HORMARGIN), dispsize[1]-(2*VERMARGIN))
        textsurf, textdict = self._generate_text_surf(text, textrect, highlight_words, fontsize)
        self._disp.blit(textsurf, (0,0))
        pygame.display.flip()
        fliptime = time.time()
        
        return fliptime, textdict
    

    def save_text_as_image(self, filename):

        pygame.image.save(self._disp, filename)
        

    
    def _generate_text_surf(self, text, textrect, highlight_words, fontsize=24):
        
        if fontsize not in self._font.keys():
            self._font[fontsize] = pygame.font.Font(self._fontfile, fontsize)
        
        # Generate a Surface for each word.
        wordsurfs = []
        text.replace('\n', ' ').replace('\r', ' ').replace('\t', '    ')
        text = text.split(' ')
        for i,word in enumerate(text):
            if word == '':
                wordsurfs.append(None)
            elif i in highlight_words:
                wordsurfs.append(self._font[fontsize].render(word, True, HLC, BGC))
            else:
                wordsurfs.append(self._font[fontsize].render(word, True, FGC, BGC))
        
        # Starting positions
        x = copy.deepcopy(textrect[0])
        y = copy.deepcopy(textrect[1])
        
        # Generate the Surface for the text display
        textdict = []
        textsurf = pygame.Surface(self._disp.get_size())
        textsurf.fill(BGC)
        for i, word in enumerate(text):
            # Skip empty words.
            if word == '':
                continue
            # Draw word
            textsurf.blit(wordsurfs[i], (x,y))
            w, h = wordsurfs[i].get_size()
            # Store word rect data in dict
            td = {'word':word, 'x':x, 'y':y, 'w':w, 'h':h, 'nr':i}
            textdict.append(td)
            # Update word position
            if i < len(text) - 1:
                x += w + SPACEWIDTH
                if x + wordsurfs[i+1].get_width() > textrect[2]:
                    y += h + LINEHEIGHT
                    x = copy.deepcopy(textrect[0])
                # TODO: Exception when textrect[3] is exceeded?
                
        return textsurf, textdict


# # # # #
# FUNCTIONS

