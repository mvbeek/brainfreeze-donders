import numpy

from constants import *


def close_to_word(x, y, word):

    left_bound = (word['x'] - (SPACEWIDTH // 2) <= x) 
    right_bound = (word['x'] + word['w'] + (SPACEWIDTH // 2) >= x)
    in_x = (left_bound.astype(int) + right_bound.astype(int)) == 2

    higher_bound = (word['y'] - (LINEHEIGHT // 2) <= y) 
    lower_bound = (word['y'] + word['h'] + (LINEHEIGHT // 2) >= y)
    in_y = (higher_bound.astype(int) + lower_bound.astype(int)) == 2


    closest = numpy.where(in_x.astype(int) + in_y.astype(int) == 2)[0]

    if len(closest) > 0:
        return closest[0]

    return None
