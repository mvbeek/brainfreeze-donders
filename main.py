# Native
import os
import sys
import time

# External
import pygame
import numpy

# Internal
from lib.constants import *

from lib.libdisplay import MagicDisplay
from lib.libkeyboard import Keyboard
from lib.libdata import generate_word_log
from lib.libanalysis import load_text_data
from lib.libhelper import close_to_word

if DUMMYMODE:
    from lib.libdummy import EyeTribe
else:
    from lib.pytribe import EyeTribe


# # # # #
# INITIALISATION

# TODO: Clear up clunky main.py by moving stuff into libraries.

# Initialise PyGame (can be called more than once.)
pygame.init()
pygame.display.init()

# Get display
disp = MagicDisplay(DISPSIZE, fullscreen=True)

# Present opening text
disp.draw_text("Loading ALL the things, please wait...")


# # # # #
# LOAD STUFF

# Check whether folders and stuff are around.


# Create a Keyboard for user interaction.
keyboard = Keyboard()

# TODO: Start parallel process(es) for data analysis in the background

# TODO: Calibration?


# # # # #
# SHOW TEXT

# Loop through all texts.
all_files = os.listdir(TEXTDIR)
highlight_words = []
for fname in all_files:

    import time
    timestamp = time.strftime('%Y%m%d-%H%M%S')

    textpath = os.path.join(TEXTDIR, fname)

    # Read text.
    with open(textpath, 'r') as textfile:
        text = textfile.read()
    fpath, ext = os.path.splitext(os.path.basename(textpath))
    fpath = os.path.join(DATADIR, fpath)
    fpath = fpath + '_' + timestamp

    # Initialise eye tracker.
    tracker = EyeTribe(logfilename=os.path.join(DATADIR, fpath + '_data'))
    time.sleep(1.0)
    
    # Start recording eye movements.
    tracker.start_recording()

    # Present text on display
    t0, textdict = disp.draw_text(text, highlight_words)
    tracker.log_message("TEXT_ONSET")
    
    # Save text data.
    generate_word_log(fpath + '_text.tsv', textdict)

    # Load the text data
    textdata = load_text_data(fpath + '_text.tsv')

    # Save screen as image
    disp.save_text_as_image(fpath + '_image.jpg')

    # Wait for participant to finish, while keeping track of whether they
    # are looking at the screen.
    missing = False
    reading = True
    keyboard.get_key(timeout=1, flush=True)
    sample_time = time.time() * 1000
    while reading:
        # Get a sample, and check whether it's valid.
        gazepos = tracker.sample()
        previous_sample_time = sample_time
        sample_time = time.time() * 1000
        pygame.mouse.set_pos(gazepos)
        if gazepos == (0,0) or gazepos == (None, None):
            if not missing:
                missing_start = time.time()
                missing = True
        else:
            pygame.mouse.set_pos(gazepos)
            if missing:
                disp.draw_border(BGC)
            # update duration for closest word
            word = close_to_word(gazepos[0], gazepos[1], textdata)
            if word:
                closest = word
                if 'dwell_time' in textdict[closest].keys():
                    textdict[closest]['dwell_time'] += sample_time - previous_sample_time
                else:
                    textdict[closest]['dwell_time'] = sample_time - previous_sample_time
                if textdict[closest]['dwell_time'] > DWELLTIMETHRESHOLD and closest not in highlight_words:
                    highlight_words.append(closest)
                    disp.draw_text(text, highlight_words)
        if missing and time.time() - missing_start > 2.0:
            disp.draw_border((255,0,0))
        # Check if the user pressed Space (indicating they are done reading.)
        key, t1 = keyboard.get_key(keylist='default', timeout=1)
        if key == pygame.K_SPACE:
            tracker.log_message("READING_DONE")
            reading = False
        time.sleep(0.1)
    
    # Clear the screen.
    disp.clear()
    
    # Stop recording eye movements
    tracker.stop_recording()


# # # # #
# CLOSE APP

# Neatly close the connection with the eye tracker.
tracker.close()
# Close the display.
disp.close()

# Un-initialise PyGame.
pygame.quit()

# Exit.
sys.exit(0)
