import os

# Eye tracker dummy?
DUMMYMODE = True

# Foreground and background colours.
BGC = (255,255,255)
FGC = (0, 0, 0)
HLC = (0, 49, 204)

# Display element sizes and margins (in pixels).
DISPSIZE = (1920, 1080)
SPACEWIDTH = 75
LINEHEIGHT = 140
HORMARGIN = 100
VERMARGIN = 100

DWELLTIMETHRESHOLD = 1500
MINFIXTEXTDIST = 800

# Log file specifics.
TEXTHEADER = ['nr', 'word', 'x', 'y', 'w', 'h']

# Folder structure.
# TODO: __file__ only works in script; add option for when app is compiled to executable.
LIBDIR = os.path.dirname(os.path.abspath(__file__))
DIR = os.path.dirname(LIBDIR)
RESDIR = os.path.join(DIR, 'resources')
TEXTDIR = os.path.join(RESDIR, 'texts')
DATADIR = os.path.join(DIR, 'data')
OUTDIR = os.path.join(DIR, 'output')
if not os.path.isdir(LIBDIR):
    raise Exception("ERROR: Couldn't find the software library! PANIC!")
if not os.path.isdir(RESDIR):
    raise Exception("ERROR: Couldn't find the resources! PANIC!")
if not os.path.isdir(TEXTDIR):
    raise Exception("ERROR: Couldn't find the texts! PANIC!")
if not os.path.isdir(DATADIR):
    os.mkdir(DATADIR)
if not os.path.isdir(OUTDIR):
    os.mkdir(OUTDIR)
