# Native

# External
import numpy
from matplotlib import pyplot

# Internal
from constants import *
from libhelper import close_to_word
from pygazeanalyser.eyetribereader import read_eyetribe
from pygazeanalyser.gazeplotter import parse_fixations, draw_fixations, \
    draw_heatmap


def load_gaze_data(fpath):
    
    raw = read_eyetribe(fpath, "TEXT_ONSET", stop="READING_DONE", \
        missing=0.0, debug=False)
    
    return raw[0]


def load_text_data(fpath):

    raw = numpy.loadtxt(fpath, dtype=str, delimiter='\t', skiprows=1, \
        unpack=True)

    textdata = {}
    for i, key in enumerate(TEXTHEADER):
        try:
            textdata[key] = raw[i].astype(float)
        except:
            textdata[key] = raw[i]

    return textdata


def calc_word_fixation_times(fname, correctlocs=True):
    
    textpath = os.path.join(DATADIR, fname + '_text.tsv')
    gazepath = os.path.join(DATADIR, fname + '_data.tsv')
    imgpath = os.path.join(DATADIR, fname + '_image.jpg')
    outpath = os.path.join(OUTDIR, fname)
    
    w = load_text_data(textpath)
    g = load_gaze_data(gazepath)
    
    f = g['events']['Efix']
    fix = parse_fixations(f)
    Mdur = numpy.nanmean(fix['dur'])
    SDdur = numpy.nanstd(fix['dur'])
    
    fig = draw_fixations(f, DISPSIZE, imagefile=imgpath, durationsize=True, \
        durationcolour=False, alpha=0.5, savefilename=outpath + '_fixplot.png')
    pyplot.close(fig)

    if correctlocs:
        gausxweight = numpy.zeros(len(f))
        for i in range(len(f)):
            closest = close_to_word(f[i][3], f[i][4], w)
            if closest is None:
                f[i][2] = 0
            else:
                gausxweight[i] = w['w'][closest].astype(float)/w['h'][closest]
                if f[i][2] <= Mdur + 2.0 * SDdur:
                    f[i][2] = 0
                f[i][3] = int(w['x'][closest] + w['w'][closest] / 2)
                f[i][4] = int(w['y'][closest] + w['h'][closest] / 2)
    else:
        gausxweight = None

    fig = draw_heatmap(f, DISPSIZE, imagefile=imgpath, \
        durationweight=True, alpha=0.5, savefilename=outpath + '_heatmap.png', \
        gausxweight=gausxweight)
    pyplot.close(fig)

