from constants import *

def generate_word_log(fpath, textdict):
    
    if len(textdict) == 0:
        return

    with open(fpath, 'w') as log:
        log.write('\t'.join(map(str, TEXTHEADER)) + '\n')
        for i, td in enumerate(textdict):
            line = []
            for key in TEXTHEADER:
                line.append(td[key])
            line = '\t'.join(map(str, line))
            log.write(line + '\n')
    
