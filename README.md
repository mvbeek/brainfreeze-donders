# README

This is a repository containing the code for the Donders Hackathon by the team Brainfreeze.

The members of the team are Marin van Beek, Edwin Dalmaijer, Vera van der Molen
and Johanna de Vos.

For more info, see:
http://www.ru.nl/dondershackathon/

## Installation

```
pip install -r requirements.txt
```

## Running a demo

Because the example data has been recorded on a 1920x1080 screen, the demo will
be slightly broken on a screen with a different screen resolution.

You can quit the demo at any time by pressing the spacebar.

During the demo words that are fixated will be highlighted and when the user
looks away from the screen a red border will appear.

To run the demo make sure `DUMMYMODE` in `lib/constants.py` is set to `True`.
Then run:

``` sh
python main.py
```

## Example data

You can find example data output by the eye tracking software in the `data`
folder and example analyses in the `output` folder.

