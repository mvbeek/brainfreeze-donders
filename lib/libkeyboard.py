import time

import pygame
import pygame.key

class Keyboard:

	def __init__(self, keylist=None, timeout=None):

		# dictionary for keynames and codes
		self.key_codes = {}
		for i in dir(pygame):
			if i[:2] == "K_":
				code = eval("pygame.%s" % i)
				name1 = pygame.key.name(code).lower()
				name2 = name1.upper()
				name3 = i[2:].lower()
				name4 = name3.upper()
				self.key_codes[name1] = code
				self.key_codes[name2] = code
				self.key_codes[name3] = code
				self.key_codes[name4] = code

		# set keyboard characteristics
		self.set_keylist(keylist)
		self.set_timeout(timeout)
	

	def set_keylist(self, keylist=None):
		
		if keylist == None or keylist == []:
			self.klist = None
		else:
			self.klist = []
			for key in keylist:
				self.klist.append(key)


	def set_timeout(self, timeout=None):

		self.timeout = timeout


	def get_key(self, keylist='default', timeout='default', flush=False):
		
		# set keylist and timeout
		if keylist == 'default':
			keylist = self.klist
		if timeout == 'default':
			timeout = self.timeout
		
		# flush if necessary
		if flush:
			pygame.event.get(pygame.KEYDOWN)
			
		# starttime
		t0 = time.time() * 1000
		t1 = time.time() * 1000

		# wait for input
		while timeout == None or t1 - t0 <= timeout:
			t1 = time.time() * 1000
			for event in pygame.event.get():
				if event.type == pygame.KEYDOWN:
					t1 = time.time()
					key = event.key
					if keylist == None or key in keylist:
						return key, t1
					
		# in case of timeout
		return None, t1
