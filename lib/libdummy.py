# Dummy EyeTribe class for testing purposes.

import time
import os

from constants import *

from libanalysis import load_gaze_data

class EyeTribe:

    """class for eye tracking and data collection using an EyeTribe tracker
    """

    def __init__(self, logfilename='default', host='localhost', port=6555):

        """Initializes an EyeTribe instance

        keyword arguments

        logfilename    --    string indicating the log file name, including
                        a full path to it's location and an extension
                        (default = 'default.txt')
        """
        
        self.gaze_data = load_gaze_data(os.path.join('data', 'dummy_data.tsv'))

    def start_recording(self):

        """Starts data recording
        """

        self.start_time = time.time()

    def stop_recording(self):

        """Stops data recording
        """

        pass

    def log_message(self, message):

        """Logs a message to the logfile, time locked to the most recent
        sample
        """
        
        pass

    def sample(self):

        """Returns the most recent point of regard (=gaze location on screen)
        coordinates (smoothed signal)

        arguments

        None

        returns

        gaze        --    a (x,y) tuple indicating the point of regard
        """

        passed_time = time.time() - self.start_time
        sample = int(passed_time / 0.03)
        if sample >= len(self.gaze_data['x']):
            return (0, 0)
        x = self.gaze_data['x'][sample] * (1920. / DISPSIZE[0])
        y = self.gaze_data['y'][sample] * (1080. / DISPSIZE[1])
        return (x, y)

    def pupil_size(self):

        """Returns the most recent pupil size sample (an average of the size
        of both pupils)

        arguments

        None

        returns

        pupsize    --    a float indicating the pupil size (in arbitrary units)
        """

        return 19.19

    def close(self):

        """Stops all data streaming, and closes both the connection to the
        tracker and the logfile
        """

        pass
